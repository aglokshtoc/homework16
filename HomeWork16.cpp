﻿#include <iostream>
#include <ctime>
#pragma warning(disable : 4996)

void vecConstruct(int vec[9][9]) {
    for (int i = 0; i < 9; i++) {
        for (int j = 0; j < 9; j++) {
            std::cout << vec[i][j] << "\t";
        } std::cout << std::endl;
    }
}
void vecSum(int vec[9][9]) {
    time_t now = time(NULL);
    tm* ltm = localtime(&now);

    int sum = 0;
    for (int j = 0; j < 9; j++) {
        sum += vec[ltm->tm_mday % 9][j];
    }
    std::cout << sum;
}

int main()
{
    int vec[9][9];
    for (int i = 0; i < 9; i++) {
        for (int j = 0; j < 9; j++) {
            vec[i][j] = i + j;
        }
    }
    vecConstruct(vec);
    vecSum(vec);
}